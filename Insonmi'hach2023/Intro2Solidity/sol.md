# writeups du chal Intro2Solidity - web3

## Contracts

### Setup.sol 
```solidity
// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.1;
import "./Challenge.sol";

contract Setup {
	Challenge public chall;

	constructor() payable {
		require(msg.value >= 100, "Not enough ETH to create the challenge..");
		chall = (new Challenge){ value: 50 ether }();
	}
	function isSolved() public view returns (bool) {
		return address(chall).balance == 0;
	}
}
```
### Challenge.sol 
```solidity
// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.1;

contract Challenge{

	constructor() payable {
    require(msg.value == 100 ether,"100ETH required for the start the challenge");
	}

  function withdraw(address payable beneficiary) public {
    beneficiary.transfer(address(this).balance);
  }
}
```
## Solution 
le but est donc d'appeler la function withdraw pour vider le contract
Pour cela on peut utiliser [remix](https://remix.ethereum.org)
il faut importer les contract et les complier et importer avec les address des Contracts
une fois les Contracts importer on peut intéragire avec le contrat Challenge et la function withdraw et une address address d'un wallet, on peut donc vérifier si le contrat a bein été vider avec la function isSolved du contract Setup et donc si le retour est True on peut donc faire une requete pour récuper le flag
